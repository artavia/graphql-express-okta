// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} 

const { PORT, HOSTNAME, OKTA_ORG_URL, OKTA_ISSUER, OKTA_CLIENT_ID, OKTA_CLIENT_SECRET, API_TOKEN_VALUE , REDIRECT_URI = `http://${HOSTNAME}:${PORT}/authorization-code/callback` } = process.env;

// =============================================
// okta variables
// =============================================
const OktaJwtVerifier = require("@okta/jwt-verifier");
const verifier = new OktaJwtVerifier( { clientId: OKTA_CLIENT_ID , issuer: OKTA_ISSUER } );

const { Client } = require("@okta/okta-sdk-nodejs");
const client = new Client( { orgUrl: OKTA_ORG_URL , token: API_TOKEN_VALUE } );

const { ExpressOIDC } = require("@okta/oidc-middleware"); 
const oidc = new ExpressOIDC( {
  issuer: OKTA_ISSUER
  , appBaseUrl: `http://${HOSTNAME}:${PORT}` // NEW 
  , client_id: OKTA_CLIENT_ID
  , client_secret: OKTA_CLIENT_SECRET
  
  // , redirect_uri: REDIRECT_URI   // OLD
  , loginRedirectUri: REDIRECT_URI  // NEW 

  , scope: "openid profile"

  , routes: {
    login: {
      path: "/access-token"
    }
    , loginCallback: {
      path: "/authorization-code/callback" // NEW
      , afterCallback: "/dashboard"
    }
  }
  
  , ensureAuthenticated: ( req,res,next ) => { 
    if(!req.userContext){ 
      console.log( "You are unauthenticated! REDIRECTING NOW!!! AVAUNT YOU SCOUNDREL!" );
      return res.status(401).json({ "errormessage" : "You are unauthenticated! "});
    }    
    res.send( req.userContext.tokens.access_token );
    next();
  }

} );

module.exports = { client, verifier, oidc };