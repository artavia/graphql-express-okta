// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
const { NODE_ENV , PORT , HOSTNAME } = process.env;
const { EXPRESS_SESSION_SECRET, SESS_NAME, SESS_LIFETIME = 1000 * 60 * 60 * 2 } = process.env;

const IS_PRODUCTION_NODE_ENV = NODE_ENV === 'production';
const IS_DEVELOPMENT_NODE_ENV = NODE_ENV === 'development';

// =============================================
// boilerplate
// =============================================
const express = require("express");
const cors = require("cors");

const { graphqlHTTP } = require("express-graphql"); 

const gql = require("graphql-tag");
const { buildASTSchema } = require("graphql");

const { v4: uuidv4 } = require("uuid");

const app = express();
app.use( cors() );

// =============================================
// session boilerplate
// =============================================
const session = require("express-session");

app.use( session( {
  name: SESS_NAME
  , secret: EXPRESS_SESSION_SECRET
  , saveUninitialized: false
  
  , resave: false
  // , resave: true

  , cookie: {
    maxAge: SESS_LIFETIME
    // , sameSite: true // 'strict'
    , sameSite: false
    // , secure: IN_PROD
  }
} ) );

// =============================================
// Import Okta and Session functionality - import and initialize
// =============================================
const okta = require("./okta");
const { oidc } = okta;
app.use( oidc.router );

// =============================================
// schema declarations
// Apparently in this context triple block quotes """ are not going to jive
// =============================================
const schema = buildASTSchema( gql`
  
  # schemas go here...

  # An exclamation point (!) after a declared field's type means "this field's value can never be null."

  # E.G. - If an array has an exclamation point after it, an array cannot be null but it can be empty.
  
  # OBJECT TYPES  
  type Post {
    id: ID
    author: Person
    body: String
  }

  type Person {
    id: ID
    posts: [Post]
    firstName: String
    lastName: String
  }
  
  # QUERY TYPES - Queries enable clients to fetch data but not to modify data. 
  type Query {
    hello: String
    posts: [Post]
    post(id: ID): Post
    authors: [Person]
    author(id: ID): Person
  }
  
  # MUTATION TYPES - To enable clients to modify data, a schema needs to define some mutations.  
  type Mutation {
    submitPost( input: PostInput! ): Post
    deletePost( id: ID! ): Boolean
  }

  input PostInput {
    id: ID
    body: String!
  }
  
` );

// =============================================
// Variable declarations
// Custom classes
// Helper functions
// =============================================
const PEOPLE = new Map();
const POSTS = new Map();

class Post {
  constructor( data ){
    Object.assign( this, data );
  }
  get author(){
    return PEOPLE.get( this.authorId );
  }
}

class Person {
  constructor( data ){
    Object.assign( this, data );
  }
  get posts(){
    return [...POSTS.values() ].filter( post => {
      return post.authorId === this.id;
    } );
  }
}

const getUserId = async ( props ) => {
  try {
    const { authorization } = props;
    const accessToken = authorization.trim().split(" ")[1];

    // const { claims: { uid } } = await okta.verifier.verifyAccessToken( accessToken );
    const verification = await okta.verifier.verifyAccessToken( accessToken , 'api://default' );
    // await console.log( "await verification", await verification );

    const { claims } = await verification;
    const { uid } = await claims;
    // await console.log( "await uid", await uid );

    return await uid;
  }
  catch( error ){
    console.log( "error" , error );
    return null;
  } 
}; 

const saveUser = async ( id ) => { 
   try {
    if( !PEOPLE.has(id) ){
      
      // const { profile: { firstName, lastName }  } = await okta.client.getUser(id);
      const user = await okta.client.getUser(id);
      // await console.log( "await user", await user );

      const { profile } = await user;
      // await console.log( "await profile", await profile );

      const { firstName, lastName } = await profile;
      // await console.log( "await firstName", await firstName );
      // await console.log( "await lastName", await lastName );
      
      PEOPLE.set( id, new Person({ id, firstName, lastName }) );
    }
  }
  catch( error ){
    console.log( "error" , error );
  }

  return PEOPLE.get( id );
}; 

// =============================================
// GraphQL Endpoints
// =============================================
const rootValue = {
  
  // Queries
  hello: () => { return "Konichiwa, you crazy and whirlled world!"; }
  
  , posts: () => { return POSTS.values(); }
  , post: ( {id} ) => { return POSTS.get(id); }
  , authors: () => { return PEOPLE.values(); }
  , author: ( {id} ) => { return PEOPLE.get(id); }

  // Mutations
  , submitPost: async ( {input}, {headers} ) => { 
    
    const authorId = await getUserId(headers);
    if( !authorId ){
      return null;
    }

    const { id = uuidv4(), body } = input;
    if( POSTS.has(id) && POSTS.get(id).authorId !== authorId ){
      return null;
    }

    await saveUser( authorId );
    POSTS.set( id, new Post( { id, authorId, body } ) );
    
    return POSTS.get(id); 
  } 

  , deletePost: async ( {id}, {headers} ) => { 

    if( !POSTS.has(id) ){
      return false;
    }

    const userId = await getUserId(headers);
    if( POSTS.get(id).authorId !== userId ){
      return false;
    }

    POSTS.delete(id);

    if( PEOPLE.get(userId).posts.length === 0 ){
      PEOPLE.delete(userId);
    }    
    return true; 
  } 

};

// =============================================
// dummy data
// =============================================
// const initializeData = () => {  
//   const fakePeople = [ { id: "1", firstName: "John", lastName: "Doe" } , { id: "2", firstName: "Jane", lastName: "Doe" } ];
//   const fakePosts = [ { id: "1", authorId: "1", body: "Konichiwa, world" }, { id: "2", authorId: "2", body: "Konichiwa, planet earth" } ];
//   fakePeople.forEach( ( person ) => {
//     const {id} = person;
//     return PEOPLE.set( id, new Person(person) );
//   } );
//   fakePosts.forEach( ( post ) => {
//     const {id} = post;
//     return POSTS.set( id, new Post(post) );
//   } );
// };
// initializeData();

// =============================================
// Server routing and listening
// =============================================

app.get( "/" , ( req, res, next ) => {  
  if(!req.userContext){
    console.log("You have reached the home page and are NOT signed in.");
    return res.status(200).send("You have reached the home page and are NOT signed in.");
  }  
  console.log( "\n\n The home page has shared mytoken \n\n" , req.userContext.tokens.access_token );
  return res.status(200).send("You have reached the home page and you ARE signed in.");
} );

app.get( "/dashboard" , oidc.ensureAuthenticated(), ( req, res, next ) => {
  if(req.userContext){
    console.log( "\n\n The dashboard page has shared mytoken \n\n" , req.userContext.tokens.access_token );
    res.status(200).send("You have reached the dashboard and are signed in.")
  }  
} );

app.get( "/logout", (req, res, next)  => { 
  req.session.destroy( err => { 
    if( err ) return res.redirect("/dashboard"); 
    req.logout(); 
    res.clearCookie( SESS_NAME ); 
    res.redirect("/"); 
  } ); 
} );

app.use( "/graphql" , graphqlHTTP( { schema, rootValue } ) );

app.listen( PORT, HOSTNAME, () => { 
  
  if( IS_PRODUCTION_NODE_ENV === true ){
    console.log( `\n Production backend is running, baby. \n`);
  }
  if( IS_DEVELOPMENT_NODE_ENV === true ){
    console.log( `\n Development backend has started at http://${HOSTNAME}:${PORT} . \n`);
  }
  if(IS_PRODUCTION_NODE_ENV === false && IS_DEVELOPMENT_NODE_ENV === false ){ 
    console.log(`\n Backend is now testing at port ${PORT} . Run and visit your frontend! \n`);
  }

  console.log(`\n Open the GraphQL playground at https://www.graphqlbin.com/v2/new and apply the endpoint \n`);
  console.log(`\n Running a GraphQL API server at http://${HOSTNAME}:${PORT}/graphql \n`);
  console.log(`\n To get authenticated VISIT http://${HOSTNAME}:${PORT}/access-token \n`);
  console.log(`\n To CRUSH your session and LOGOUT go and VISIT http://${HOSTNAME}:${PORT}/logout \n`);
} );